require 'rubygems'
require 'mongoid'
require 'haml'
require 'sinatra'
require './model/lorem'
require './model/author'
require 'json'

Mongoid.load!("./mongoid.yml")

#output_text(3, "cormac") { sentencify(@author_sample) }

get '/' do
  @author = "initial"
  haml :index
end

post '/' do
  @author = Author.where(:name.all => [params[:button]]).pluck(:text)[0]
  haml :index
end