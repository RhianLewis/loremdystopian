require './main.rb'

class Author
  include Mongoid::Document
  field :name, :type => String
  field :text, :type => String
  attr_accessible :name, :text
end